package com.poccofinance.loan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({ LoanApplicationConfig.class})
public class PoccoApp {

	public static void main(String[] args) {
		SpringApplication.run(PoccoApp.class, args);
	}
}
