package com.poccofinance.loan.domain.dto;

import com.poccofinance.loan.domain.LoanApplicationStatus;
import com.poccofinance.loan.domain.LoanType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

public class LoanApplication {
    private String customerId;
    private Optional<String> loanId = Optional.empty();
    private LoanType loanType = LoanType.SIMPLE;
    private BigDecimal amount;
    private int termInDays;
    private LoanApplicationStatus status = LoanApplicationStatus.NEW;
    private LocalDateTime timestamp;

    public LoanApplication(String customerId, BigDecimal amount, int termInDays) {
        setCustomerId(customerId);
        setAmount(amount);
        setTermInDays(termInDays);
        setTimestamp(LocalDateTime.now());
    }

    public LoanApplication withType(LoanType type) {
        setLoanType(type);
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Optional<String> getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = Optional.of(loanId);
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getTermInDays() {
        return termInDays;
    }

    public void setTermInDays(int termInDays) {
        this.termInDays = termInDays;
    }

    public LoanApplicationStatus getStatus() {
        return status;
    }

    public void setStatus(LoanApplicationStatus status) {
        this.status = status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
