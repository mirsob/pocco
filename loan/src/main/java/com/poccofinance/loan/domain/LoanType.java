package com.poccofinance.loan.domain;

import java.math.BigDecimal;
import java.math.MathContext;

public enum LoanType {
    SIMPLE(BigDecimal.valueOf(1000.0), BigDecimal.valueOf(10000.0), 30, 1080, 10, 30);

    public static final MathContext MC_PRECISION = new MathContext(4);

    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private int minTermDays;
    private int maxTermDays;
    private double principalFactor;
    private int extendIntervalInDays;

    LoanType(BigDecimal minAmount, BigDecimal maxAmount, int minTermDays, int maxTermDays, double principalFactorInPercent, int extendIntervalInDays){
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.minTermDays = minTermDays;
        this.maxTermDays = maxTermDays;
        this.principalFactor = principalFactorInPercent/100;
        this.extendIntervalInDays = extendIntervalInDays;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public int getMinTermDays() {
        return minTermDays;
    }

    public int getMaxTermDays() {
        return maxTermDays;
    }

    public double getPrincipalFactor() {
        return principalFactor;
    }

    public int getExtendIntervalInDays() {
        return extendIntervalInDays;
    }
}
