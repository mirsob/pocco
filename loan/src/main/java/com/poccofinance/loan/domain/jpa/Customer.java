package com.poccofinance.loan.domain.jpa;

import java.util.HashSet;
import java.util.Set;

public class Customer {
    private String id;
    private Set<Loan> loans = new HashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<Loan> getLoans() {
        return loans;
    }

    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }
}
