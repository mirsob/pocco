package com.poccofinance.loan.domain.dto;

import com.poccofinance.loan.domain.LoanApplicationStatus;

import java.io.Serializable;

public class LoanRequestProcessingResult implements Serializable{
    private String loanId;
    private LoanApplicationStatus applicationStatus;

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public LoanApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(LoanApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
}
