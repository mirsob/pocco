package com.poccofinance.loan.domain;

public enum ExtendRequestStatus {
    EXTENDED, REJECTED
}
