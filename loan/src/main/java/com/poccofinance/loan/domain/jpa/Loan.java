package com.poccofinance.loan.domain.jpa;

import com.poccofinance.loan.domain.LoanType;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.poccofinance.loan.domain.LoanType.SIMPLE;

public abstract class Loan {

    private String id;
    private Customer customer;
    private BigDecimal amount;
    private BigDecimal principal;
    private int termInDays;
    private LocalDate dueDate = LocalDate.MAX;
    private LocalDate approvedDate;
    private int extendTermInDays;

    public Loan(){
    }

    public Loan(BigDecimal amount, int termInDays) {
        setAmount(amount);
        setPrincipal(amount.multiply(BigDecimal.valueOf(SIMPLE.getPrincipalFactor()), SIMPLE.MC_PRECISION));
        setTermInDays(termInDays);
        setApprovedDate(LocalDate.now());
        setDueDate(approvedDate.plusDays(termInDays));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    abstract LoanType getType();

    abstract int extend();

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPrincipal() {
        return principal;
    }

    public void setPrincipal(BigDecimal principal) {
        this.principal = principal;
    }

    public int getTermInDays() {
        return termInDays;
    }

    public void setTermInDays(int termInDays) {
        this.termInDays = termInDays;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDate approvedDate) {
        this.approvedDate = approvedDate;
    }

    public int getExtendTermInDays() {
        return extendTermInDays;
    }

    public void setExtendTermInDays(int extendTermInDays) {
        this.extendTermInDays = extendTermInDays;
    }
}
