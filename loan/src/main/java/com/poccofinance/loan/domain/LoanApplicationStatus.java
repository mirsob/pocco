package com.poccofinance.loan.domain;

public enum LoanApplicationStatus {
    NEW, UNDER_PROCESSING, APPROVED, REJECTED
}
