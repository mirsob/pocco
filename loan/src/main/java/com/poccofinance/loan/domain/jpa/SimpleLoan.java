package com.poccofinance.loan.domain.jpa;

import com.poccofinance.loan.domain.LoanType;

import java.math.BigDecimal;

import static com.poccofinance.loan.domain.LoanType.SIMPLE;

public class SimpleLoan extends Loan {

    public SimpleLoan(){

    }

    public SimpleLoan(BigDecimal amount, int termInDays) {
        super(amount, termInDays);
    }

    @Override
    public LoanType getType() {
        return SIMPLE;
    }

    @Override
    public int extend() {
        setDueDate(getDueDate().plusDays(SIMPLE.getExtendIntervalInDays()));
        setExtendTermInDays(getExtendTermInDays() + SIMPLE.getExtendIntervalInDays());
        return SIMPLE.getExtendIntervalInDays();
    }
}
