package com.poccofinance.loan.domain.dto;

import com.poccofinance.loan.domain.LoanType;

public class LoanRequest {
    private String customerId;
    private LoanType loanType = LoanType.SIMPLE;
    private String amount;
    private Integer termInDays;

    public LoanRequest(){

    }

    public LoanRequest(String customerId, String amount, Integer termInDays){
        setCustomerId(customerId);
        setAmount(amount);
        setTermInDays(termInDays);
    }

    public LoanRequest withType(LoanType type) {
        setLoanType(type);
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getTermInDays() {
        return termInDays;
    }

    public void setTermInDays(Integer termInDays) {
        this.termInDays = termInDays;
    }
}
