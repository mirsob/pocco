package com.poccofinance.loan;

import com.poccofinance.loan.controller.LoanProcessorController;
import com.poccofinance.loan.persistance.DummyCustomerRepository;
import com.poccofinance.loan.persistance.DummyLoanRepository;
import com.poccofinance.loan.service.LoanApplicationProcessingServiceImpl;
import com.poccofinance.loan.service.DefaultTimeProvider;
import com.poccofinance.loan.service.LoanValidatorImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        LoanValidatorImpl.class,
        DummyLoanRepository.class,
        DummyCustomerRepository.class,
        DefaultTimeProvider.class,
        LoanApplicationProcessingServiceImpl.class,
        LoanProcessorController.class
})
public class LoanApplicationConfig {
}
