package com.poccofinance.loan.controller;

import com.poccofinance.loan.domain.ExtendRequestStatus;
import com.poccofinance.loan.domain.LoanApplicationStatus;
import com.poccofinance.loan.domain.dto.LoanApplication;
import com.poccofinance.loan.domain.dto.LoanRequest;
import com.poccofinance.loan.domain.dto.LoanRequestProcessingResult;
import com.poccofinance.loan.service.api.LoanApplicationProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class LoanProcessorController {

    @Autowired
    private LoanApplicationProcessingService processingService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        String info = "Test application to apply for a simple loan <br> " +
                "To request the loan call /applyforloan (POST)<br>" +
                "To extend the loan call /extendloan/{loanId} (POST)<br>";

        return info;
    }

    @RequestMapping(value = "/applyforloan/", method = RequestMethod.POST)
    public ResponseEntity<LoanRequestProcessingResult> applyforloan(@RequestBody LoanRequest loanRequest) {

        BigDecimal amountBd = new BigDecimal(loanRequest.getAmount());
        LoanApplication loanApplication = new LoanApplication(loanRequest.getCustomerId(), amountBd, loanRequest.getTermInDays());
        LoanApplicationStatus status = processingService.processApplication(loanApplication);

        LoanRequestProcessingResult processingResult = new LoanRequestProcessingResult();
        processingResult.setApplicationStatus(status);
        loanApplication.getLoanId().ifPresent(id -> processingResult.setLoanId(id));

        return new ResponseEntity<LoanRequestProcessingResult>(processingResult, HttpStatus.OK);
    }

    @RequestMapping(value = "/extendloan/{loanId}", method = RequestMethod.POST)
    public ResponseEntity<ExtendRequestStatus>  extendloan(@PathVariable String loanId) {
        ExtendRequestStatus status = ExtendRequestStatus.REJECTED;
        if(processingService.extendLoanForFixedPeriode(loanId)) {
            status = ExtendRequestStatus.EXTENDED;
        }
        return new ResponseEntity<ExtendRequestStatus>(status, HttpStatus.OK);
    }
}
