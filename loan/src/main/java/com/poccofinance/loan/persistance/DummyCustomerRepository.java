package com.poccofinance.loan.persistance;

import com.poccofinance.loan.domain.jpa.Customer;
import com.poccofinance.loan.persistance.api.CustomersRepository;

import java.util.HashMap;
import java.util.Optional;

public class DummyCustomerRepository implements CustomersRepository {
    private HashMap<String, Customer> dataHolder  = new HashMap<>();

    @Override
    public Optional<Customer> findById(String id) {
        return Optional.ofNullable(dataHolder.get(id));
    }

    @Override
    public void addOrMerge(Customer customer) {
        dataHolder.put(customer.getId(), customer);
    }
}
