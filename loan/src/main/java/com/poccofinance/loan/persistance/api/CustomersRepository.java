package com.poccofinance.loan.persistance.api;

import com.poccofinance.loan.domain.jpa.Customer;

import java.util.Optional;

public interface CustomersRepository {

    Optional<Customer> findById(String id);

    void addOrMerge(Customer customer);
}
