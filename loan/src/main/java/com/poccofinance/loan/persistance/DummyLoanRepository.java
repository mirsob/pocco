package com.poccofinance.loan.persistance;

import com.poccofinance.loan.domain.jpa.Loan;
import com.poccofinance.loan.persistance.api.LoanRepository;

import java.util.HashMap;
import java.util.Optional;

public class DummyLoanRepository implements LoanRepository {
    private HashMap<String, Loan> dataHolder  = new HashMap<>();

    @Override
    public Optional<Loan> findById(String id) {
        return Optional.ofNullable(dataHolder.get(id));
    }

    @Override
    public void addOrMerge(Loan loan) {
        dataHolder.put(loan.getId(), loan);
    }
}
