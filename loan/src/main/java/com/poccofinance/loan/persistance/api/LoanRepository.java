package com.poccofinance.loan.persistance.api;

import com.poccofinance.loan.domain.jpa.Loan;

import java.util.Optional;

public interface LoanRepository {
    Optional<Loan> findById(String id);

    void addOrMerge(Loan loan);
}
