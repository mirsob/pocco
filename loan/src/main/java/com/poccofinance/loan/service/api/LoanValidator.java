package com.poccofinance.loan.service.api;

import com.poccofinance.loan.domain.LoanType;
import com.poccofinance.loan.domain.dto.LoanApplication;

import java.math.BigDecimal;

public interface LoanValidator {
    boolean isAmountInRange(LoanType loanType, BigDecimal amount);

    boolean isTermInRange(LoanType loanType, int term);

    boolean isTimeAndAmountRestricted(LoanApplication loanApplication);

    boolean isValid(LoanApplication loanApplication);
}
