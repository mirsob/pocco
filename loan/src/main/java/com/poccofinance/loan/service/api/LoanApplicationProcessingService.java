package com.poccofinance.loan.service.api;

import com.poccofinance.loan.domain.dto.LoanApplication;
import com.poccofinance.loan.domain.LoanApplicationStatus;

public interface LoanApplicationProcessingService {
    LoanApplicationStatus processApplication(LoanApplication loanApplication);

    boolean extendLoanForFixedPeriode(String loanId);
}
