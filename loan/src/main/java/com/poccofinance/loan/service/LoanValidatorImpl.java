package com.poccofinance.loan.service;

import com.poccofinance.loan.domain.LoanType;
import com.poccofinance.loan.domain.dto.LoanApplication;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Service
public class LoanValidatorImpl implements com.poccofinance.loan.service.api.LoanValidator {

    @Override
    public boolean isAmountInRange(LoanType loanType, BigDecimal amount) {
        return (loanType.getMinAmount().compareTo(amount) == -1 || loanType.getMinAmount().compareTo(amount) == 0)
                &&  (loanType.getMaxAmount().compareTo(amount) == 1 || loanType.getMaxAmount().compareTo(amount) == 0);
    }

    @Override
    public boolean isTermInRange(LoanType loanType, int term) {
        return (loanType.getMinTermDays() <= term) && (term <= loanType.getMaxTermDays());
    }

    @Override
    public boolean isTimeAndAmountRestricted(LoanApplication loanApplication) {
        LocalDateTime timestamp = loanApplication.getTimestamp();
        if(timestamp == null) {
            return true;
        }
        boolean timeRestriction = timestamp.toLocalTime().isAfter(LocalTime.of(0, 0))
                && timestamp.toLocalTime().isBefore(LocalTime.of(6, 0));

        return timeRestriction && (loanApplication.getAmount().compareTo(loanApplication.getLoanType().getMaxAmount()) == 0);
    }

    @Override
    public boolean isValid(LoanApplication loanApplication) {
        return isAmountInRange(loanApplication.getLoanType(), loanApplication.getAmount())
                && isTermInRange(loanApplication.getLoanType(), loanApplication.getTermInDays())
                && !isTimeAndAmountRestricted(loanApplication);
    }
}
