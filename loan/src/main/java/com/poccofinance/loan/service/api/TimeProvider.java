package com.poccofinance.loan.service.api;

import java.time.LocalDateTime;

public interface TimeProvider {

    LocalDateTime now();
}
