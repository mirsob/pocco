package com.poccofinance.loan.service;

import com.poccofinance.loan.service.api.TimeProvider;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DefaultTimeProvider implements TimeProvider {
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
