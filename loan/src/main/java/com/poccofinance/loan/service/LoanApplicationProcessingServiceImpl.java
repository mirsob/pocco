package com.poccofinance.loan.service;

import com.poccofinance.loan.domain.*;
import com.poccofinance.loan.domain.dto.LoanApplication;
import com.poccofinance.loan.domain.jpa.Customer;
import com.poccofinance.loan.domain.jpa.Loan;
import com.poccofinance.loan.domain.jpa.SimpleLoan;
import com.poccofinance.loan.persistance.api.CustomersRepository;
import com.poccofinance.loan.persistance.api.LoanRepository;
import com.poccofinance.loan.service.api.LoanApplicationProcessingService;
import com.poccofinance.loan.service.api.LoanValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class LoanApplicationProcessingServiceImpl implements LoanApplicationProcessingService {

    @Autowired
    private LoanValidator loanValidator;
    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private LoanRepository loanRepository;

    @Override
    public LoanApplicationStatus processApplication(LoanApplication loanApplication) {
        switch (loanApplication.getLoanType()) {
            case SIMPLE:
                processSimpleLoanApplication(loanApplication);
                break;
                default:
        }
        return loanApplication.getStatus();
    }

    @Override
    public boolean extendLoanForFixedPeriode(String loanId) {
        boolean extended = false;
        if(loanRepository.findById(loanId).isPresent()) {
            extended = true;
        }
        return extended;
    }

    private void processSimpleLoanApplication(LoanApplication loanApplication) {
        loanApplication.setStatus(LoanApplicationStatus.UNDER_PROCESSING);

        if(!loanValidator.isValid(loanApplication)) {
            loanApplication.setStatus(LoanApplicationStatus.REJECTED);
            return;
        }

        Loan loan = persistApplicationData(loanApplication);
        loanApplication.setLoanId(loan.getId());

        loanApplication.setStatus(LoanApplicationStatus.APPROVED);
    }

    private Loan persistApplicationData(LoanApplication loanApplication) {
        Customer customer = customersRepository.findById(loanApplication.getCustomerId()).orElseGet(() -> {
            Customer cust = new Customer();
            cust.setId(loanApplication.getCustomerId());
            return cust;
        });

        SimpleLoan loan = new SimpleLoan(loanApplication.getAmount(), loanApplication.getTermInDays());
        loan.setId(UUID.randomUUID().toString());
        loan.setCustomer(customer);
        customer.getLoans().add(loan);

        customersRepository.addOrMerge(customer);
        loanRepository.addOrMerge(loan);

        return loan;
    }
}
