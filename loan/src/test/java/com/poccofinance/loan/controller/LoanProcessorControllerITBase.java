package com.poccofinance.loan.controller;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;

public abstract class LoanProcessorControllerITBase {
    @LocalServerPort
    protected int port;
    protected TestRestTemplate restTemplate = new TestRestTemplate();
    protected HttpHeaders headers = new HttpHeaders();

    protected String getUrl(String uri) {
        return "http://localhost:" + port + uri;
    }
}
