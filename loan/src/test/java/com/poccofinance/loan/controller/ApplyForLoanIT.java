package com.poccofinance.loan.controller;

import com.poccofinance.loan.PoccoApp;
import com.poccofinance.loan.domain.ExtendRequestStatus;
import com.poccofinance.loan.domain.LoanApplicationStatus;
import com.poccofinance.loan.domain.LoanType;
import com.poccofinance.loan.domain.dto.LoanRequest;
import com.poccofinance.loan.domain.dto.LoanRequestProcessingResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PoccoApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplyForLoanIT extends LoanProcessorControllerITBase  {

    @Test
    public void callRoot_putEmptyQuery_expectInfoString() {
        //given
        headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        //when
        ResponseEntity<String> response = restTemplate.exchange(getUrl("/"), HttpMethod.GET,
                entity, String.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void callApplyforsimpleloan_putValidRequest_expectStatusAccepted() {
        //given
        headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        LoanRequest loanRequest = new LoanRequest("customerId1", "1050.0", 365)
                .withType(LoanType.SIMPLE);

        HttpEntity<LoanRequest> entity = new HttpEntity<LoanRequest>(loanRequest, headers);

        //when
        ResponseEntity<LoanRequestProcessingResult> response = restTemplate.exchange(getUrl("/applyforloan/"), HttpMethod.POST,
                entity, LoanRequestProcessingResult.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getApplicationStatus()).isEqualTo(LoanApplicationStatus.APPROVED);
        assertThat(response.getBody().getLoanId()).isNotEmpty();
    }

}
