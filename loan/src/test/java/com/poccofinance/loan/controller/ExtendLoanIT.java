package com.poccofinance.loan.controller;

import com.poccofinance.loan.PoccoApp;
import com.poccofinance.loan.domain.ExtendRequestStatus;
import com.poccofinance.loan.domain.LoanType;
import com.poccofinance.loan.domain.dto.LoanRequest;
import com.poccofinance.loan.domain.dto.LoanRequestProcessingResult;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PoccoApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ExtendLoanIT extends LoanProcessorControllerITBase {

    private String loanId;

    @Before
    public void setUp() {
        headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        LoanRequest loanRequest = new LoanRequest("customerId1", "1050.0", 365)
                .withType(LoanType.SIMPLE);

        loanId = restTemplate.exchange(getUrl("/applyforloan/"), HttpMethod.POST, new HttpEntity<LoanRequest>(loanRequest, headers), LoanRequestProcessingResult.class)
                .getBody().getLoanId();

    }

    @Test
    public void callExtendloan_putValidLoanID_expectExtended() {
        //given
        headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        //when
        ResponseEntity<ExtendRequestStatus> response = restTemplate.exchange(getUrl("/extendloan/" + loanId), HttpMethod.POST,
                entity, ExtendRequestStatus.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(ExtendRequestStatus.EXTENDED);
    }

}
