package com.poccofinance.loan.service;

import com.poccofinance.loan.domain.LoanApplicationStatus;
import com.poccofinance.loan.domain.LoanType;
import com.poccofinance.loan.domain.dto.LoanApplication;
import com.poccofinance.loan.persistance.api.CustomersRepository;
import com.poccofinance.loan.persistance.api.LoanRepository;
import com.poccofinance.loan.service.api.LoanApplicationProcessingService;
import com.poccofinance.loan.service.api.LoanValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = LoanApplicationProcessingServiceImplBehaviourTest.TestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class LoanApplicationProcessingServiceImplBehaviourTest {

    @Autowired
    private LoanValidator loanValidator;
    @Autowired
    private LoanApplicationProcessingService underTest;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(loanValidator.isValid(any())).thenReturn(true);
    }

    @Test
    public void callProcessApplication_provideAnyApplication_expectSequenceOfCalls() {
        //given
        LoanApplication tstApplication = new LoanApplication("custId1", BigDecimal.valueOf(1500.5), 720)
                .withType(LoanType.SIMPLE);
        tstApplication.setTimestamp(LocalDateTime.of(2018, 1, 10, 8, 00));
        //when
        LoanApplicationStatus processingStatus = underTest.processApplication(tstApplication);
        //then
        verify(loanValidator).isValid(any());
    }

    @Configuration
    @Import({
            LoanApplicationProcessingServiceImpl.class
    })
    static class TestConfig {

        @Bean
        CustomersRepository customersRepository() {
            return mock(CustomersRepository.class);
        }

        @Bean
        LoanRepository loanRepository() {
            return mock(LoanRepository.class);
        }

        @Bean
        LoanValidator loanValidator() {
            return mock(LoanValidator.class);
        }
    }
}
