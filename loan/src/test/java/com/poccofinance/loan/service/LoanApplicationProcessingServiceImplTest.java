package com.poccofinance.loan.service;

import com.poccofinance.loan.domain.LoanApplicationStatus;
import com.poccofinance.loan.domain.LoanType;
import com.poccofinance.loan.domain.dto.LoanApplication;
import com.poccofinance.loan.domain.jpa.Customer;
import com.poccofinance.loan.domain.jpa.Loan;
import com.poccofinance.loan.domain.jpa.SimpleLoan;
import com.poccofinance.loan.persistance.api.CustomersRepository;
import com.poccofinance.loan.persistance.api.LoanRepository;
import com.poccofinance.loan.service.api.LoanApplicationProcessingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@ContextConfiguration(classes = LoanApplicationProcessingServiceImplTest.TestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class LoanApplicationProcessingServiceImplTest {

    private Customer anyCustomer;
    private String anyCustomerId;
    private String anyLoanId;
    private Loan anyLoan;

    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private LoanApplicationProcessingService underTest;

    @Before
    public void setUp() throws Exception {
        anyCustomerId = "customerId1";
        anyCustomer = new Customer();
        anyCustomer.setId(anyCustomerId);
        when(customersRepository.findById(anyString())).thenReturn(Optional.of(anyCustomer));

        anyLoanId = "loanId1";
        anyLoan = new SimpleLoan(BigDecimal.valueOf(1500.5), 720);
        anyLoan.setCustomer(anyCustomer);
        anyCustomer.getLoans().add(anyLoan);
        when(loanRepository.findById(anyString())).thenReturn(Optional.of(anyLoan));
    }

    @Test
    public void callProcessApplication_provideValidApplication_expectAccepted() {
        //given
        LoanApplication tstApplication = new LoanApplication("custId1", BigDecimal.valueOf(1500.5), 720)
                .withType(LoanType.SIMPLE);
        tstApplication.setTimestamp(LocalDateTime.of(2018, 1, 10, 8, 00));
        //when
        LoanApplicationStatus processingStatus = underTest.processApplication(tstApplication);
        //then
        assertThat(processingStatus).isEqualTo(LoanApplicationStatus.APPROVED);
    }

    @Test
    public void callProcessApplication_provideInvalidAmount_expectRejected() {
        //given
        LoanApplication tstApplication = new LoanApplication("custId1", BigDecimal.valueOf(100000.0), 720)
                .withType(LoanType.SIMPLE);
        tstApplication.setTimestamp(LocalDateTime.of(2018, 1, 10, 8, 00));
        //when
        LoanApplicationStatus processingStatus = underTest.processApplication(tstApplication);
        //then
        assertThat(processingStatus).isEqualTo(LoanApplicationStatus.REJECTED);
    }

    @Test
    public void callProcessApplication_provideMaxValidAmountAndApplicationTime300_expectRejected() {
        //given
        LoanApplication tstApplication = new LoanApplication("custId1", LoanType.SIMPLE.getMaxAmount(), 720)
                .withType(LoanType.SIMPLE);
        tstApplication.setTimestamp(LocalDateTime.of(2018, 1, 10, 3, 00));
        //when
        LoanApplicationStatus processingStatus = underTest.processApplication(tstApplication);
        //then
        assertThat(processingStatus).isEqualTo(LoanApplicationStatus.REJECTED);
    }

    @Test
    public void callProcessApplication_provideMaxValidAmountAndApplicationTime1100_expectAccepted() {
        //given
        LoanApplication tstApplication = new LoanApplication("custId1", LoanType.SIMPLE.getMaxAmount(), 720)
                .withType(LoanType.SIMPLE);
        tstApplication.setTimestamp(LocalDateTime.of(2018, 1, 10, 11, 00));
        //when
        LoanApplicationStatus processingStatus = underTest.processApplication(tstApplication);
        //then
        assertThat(processingStatus).isEqualTo(LoanApplicationStatus.APPROVED);
    }

    @Configuration
    @Import({
            LoanValidatorImpl.class,
            LoanApplicationProcessingServiceImpl.class
    })
    static class TestConfig {

        @Bean
        CustomersRepository customersRepository() {
            return mock(CustomersRepository.class);
        }

        @Bean
        LoanRepository loanRepository() {
            return mock(LoanRepository.class);
        }
    }
}


